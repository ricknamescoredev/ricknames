package org.joeypizza.ricknames;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.app.ListActivity;
import android.widget.EditText;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends ListActivity {

    private RicknamesDataSource datasource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RicknameApplication app = (RicknameApplication) getApplicationContext();

        datasource = app.getDatasource();
        datasource.open();
        datasource.initialize(this.getResources());

        //use SimpleCursorAdapter to show the elements
        //in a ListView
        ArrayAdapter<Rickname> adapter = new ArrayAdapter<Rickname>(this,
                android.R.layout.simple_list_item_1, new ArrayList<Rickname>());
        setListAdapter(adapter);
    }

    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Rickname> adapter = (ArrayAdapter<Rickname>) getListAdapter();
        Rickname rickname = null;
        switch (view.getId()) {
            case R.id.add:
                Intent i = new Intent(getApplicationContext(), AddRicknameActivity.class);

                startActivity(i);
                break;
            case R.id.random:
                ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                //TODO: use our own rickname generation service. This code is
                //here now just to prove out that we can make an http GET request
                //against a String-providing API. Remove 'false' condition to test
                if (false && networkInfo != null && networkInfo.isConnected()) {
                    new GetRicknameTask().execute("http://randomword.setgetgo.com/get.php");
                    break;
                }
                else {
                    rickname = datasource.getRandom();
                    if (rickname != null) {
                        adapter.clear();
                        adapter.add(rickname);
                    }
                }
                break;
            case R.id.see_all:
                Intent seeAll = new Intent(getApplicationContext(), SeeAllActivity.class);
                startActivity(seeAll);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        datasource.open();
        ArrayAdapter<Rickname> dumb = (ArrayAdapter<Rickname>) getListAdapter();
        dumb.clear();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

    private class GetRicknameTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String ... urls) {
            try {
                return getRickname(urls[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            ArrayAdapter<Rickname> adapter = (ArrayAdapter<Rickname>) getListAdapter();
            adapter.clear();
            adapter.add(datasource.createRickname(result));
        }

        private String getRickname(String myUrl) throws IOException {
            InputStream is = null;
            int maxLen = 50;

            try {
                URL url = new URL(myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("DEBUG", "The response is: " + response);
                is = conn.getInputStream();

                String contentAsString = readIt(is, maxLen);
                return contentAsString;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            reader.read(buffer);
            return new String(buffer);
        }
    }
}
