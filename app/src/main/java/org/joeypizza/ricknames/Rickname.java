package org.joeypizza.ricknames;

/**
 * Created by doakl on 6/21/2016.
 */
public class Rickname {
    private long id;
    private String rickname;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRickname() {
        return rickname;
    }

    public void setRickname(String rickname) {
        this.rickname = rickname;
    }

    @Override
    public String toString() {
        return rickname;
    }
}
