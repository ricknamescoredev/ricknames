package org.joeypizza.ricknames;

import android.app.Application;

/**
 * Created by doakl on 6/22/2016.
 */
public class RicknameApplication extends Application{
    private RicknamesDataSource datasource;

    public RicknamesDataSource getDatasource() {
        return datasource;
    }

    public void setDatasource(RicknamesDataSource source) {
        datasource = source;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        datasource = new RicknamesDataSource(this);
        datasource.open();
        datasource.initialize(this.getResources());
    }
}
