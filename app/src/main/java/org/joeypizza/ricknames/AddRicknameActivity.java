package org.joeypizza.ricknames;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AddRicknameActivity extends ListActivity {

    private RicknamesDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rickname);
        Intent i = getIntent();
        RicknameApplication app = (RicknameApplication) getApplicationContext();

        //use SimpleCursorAdapter to show the elements
        //in a ListView
        ArrayAdapter<Rickname> adapter = new ArrayAdapter<Rickname>(this,
                android.R.layout.simple_list_item_1, new ArrayList<Rickname>());
        setListAdapter(adapter);

        datasource = app.getDatasource();
        datasource.open();
    }
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Rickname> adapter = (ArrayAdapter<Rickname>) getListAdapter();
        Rickname rickname = null;
        switch (view.getId()) {
            case R.id.addToDb:
                EditText rick = (EditText) findViewById(R.id.new_rickname);
                if(StringUtils.isEmpty( rick.getText().toString())) {
                    Log.w(MainActivity.class.toString(), "Empty rick, skipping");
                    break;
                }
                rickname = datasource.createRickname(rick.getText().toString());
                adapter.clear();
                adapter.add(rickname);
                ((EditText) findViewById(R.id.new_rickname)).setText("");
                break;
            case R.id.random:
                rickname = datasource.getRandom();
                if (rickname != null) {
                    adapter.clear();
                    adapter.add(rickname);
                }
                break;
            case R.id.see_all:
                List<Rickname> allRicks= datasource.getAllRicknames();
                adapter.clear();
                adapter.addAll(allRicks);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

}
