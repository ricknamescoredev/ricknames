package org.joeypizza.ricknames;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

public class SeeAllActivity extends ListActivity {
    private RicknamesDataSource datasource;
    private List<Rickname> allRicks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all);

        RicknameApplication app = (RicknameApplication) getApplicationContext();

        //use SimpleCursorAdapter to show the elements
        //in a ListView
        ArrayAdapter<Rickname> adapter = new ArrayAdapter<Rickname>(this,
                android.R.layout.simple_list_item_1, new ArrayList<Rickname>());
        setListAdapter(adapter);

        datasource = app.getDatasource();
        datasource.open();
        allRicks= datasource.getAllRicknames();
        adapter.clear();
        adapter.addAll(allRicks);

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        ToggleButton tButt = (ToggleButton) findViewById(R.id.delete_toggle);
        if(tButt.isChecked()) {
            super.onListItemClick(l, v, position, id);
            final Rickname clickedName = allRicks.get(position);
            datasource.deleteRickname(clickedName);
            allRicks.remove(position);
            ArrayAdapter<Rickname> adapter = (ArrayAdapter<Rickname>) getListAdapter();
            adapter.remove(clickedName);
        }

    }


}
