package org.joeypizza.ricknames;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by doakl on 6/21/2016.
 */
public class RicknamesDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
        MySQLiteHelper.COLUMN_RICKNAME };

    public RicknamesDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void initialize(Resources resources) {
        Cursor cursor = database.query(MySQLiteHelper.TABLE_RICKNAMES, allColumns,
                null, null, null, null, null);

        if(!cursor.moveToFirst()) {
            String[] ricknames = resources.getStringArray(R.array.ricks);
            //String[] ricknames = new String[]{"Pepperoni Rick", "The Yee Haw Pee Paw", "Poops Buttman"};
            for(String rickname : ricknames) {
                createRickname(rickname);
            }
        }

    }

    public Rickname createRickname(String rickname) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_RICKNAME, rickname);
        long insertId = database.insert(MySQLiteHelper.TABLE_RICKNAMES, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_RICKNAMES, allColumns,
                MySQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Rickname newRickname = cursorToRickname(cursor);
        cursor.close();
        return newRickname;
    }

    public void deleteRickname(Rickname rickname) {
        long id = rickname.getId();
        System.out.println("Rickname deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_RICKNAMES, MySQLiteHelper.COLUMN_ID
        + " = " + id, null);
    }

    public Rickname getRandom() {
        Rickname rick = null;

        Cursor cursor = database.query(MySQLiteHelper.TABLE_RICKNAMES, allColumns,
                null, null, null, null, null);
        Random generator = new Random(System.currentTimeMillis());
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(generator.nextInt(cursor.getCount()));
            if (!cursor.isAfterLast()) {
                rick = cursorToRickname(cursor);
            }
        }
        cursor.close();
        return rick;
    }

    public List<Rickname> getAllRicknames() {
        List<Rickname> ricknames = new ArrayList<>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_RICKNAMES, allColumns,
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Rickname rickname = cursorToRickname(cursor);
            ricknames.add(rickname);
            cursor.moveToNext();
        }
        cursor.close();
        return ricknames;
    }

    private Rickname cursorToRickname(Cursor cursor) {
        Rickname rickname = new Rickname();
        rickname.setId(cursor.getLong(0));
        rickname.setRickname(cursor.getString(1));
        return rickname;
    }
}
